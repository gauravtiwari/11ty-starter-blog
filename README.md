## Rails blog on Chuspace [LIVE DEMO](https://foo.com/)

There’s a simple secret to building faster websites. Unfortunately, modern web development has been trending in the opposite direction—towards more. More JavaScript, more features, more moving parts, and ultimately more complexity needed to keep it all running smoothly.
